use chrono::{Local, Timelike};
use chrono_tz::Europe::Berlin;
// use forecast::{ApiClient, ApiResponse, ForecastRequestBuilder};
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
// use lazy_static;
// use reqwest::Client;
// use serde_derive::Deserialize;
use std::env;
// use std::fs;
// use std::sync::{Arc, Mutex};
use std::{convert::Infallible, net::SocketAddr};
// use toml;

// lazy_static::lazy_static! {
//     static ref CONFIG: Config = toml::from_str(
//         &fs::read_to_string("config.toml").expect("Something went wrong when reading config.toml."),
//     )
//     .expect("config.toml is not valid toml!");
//     static ref CURRENT_RESPONSE: Arc<Mutex<Option<ApiResponse>>> = Arc::new(Mutex::new(None));
// }

// #[derive(Deserialize)]
// struct Config {
//     api_key: String,
//     lat: f64,
//     long: f64,
// }

async fn handle(_: Request<Body>) -> Result<Response<Body>, Infallible> {
    let current_time = Local::now().with_timezone(&Berlin);
    let time_as_string = String::from(current_time.to_rfc3339());
    
    let seconds = (current_time.num_seconds_from_midnight()) as f64;

    let r = (f64::sin(seconds / 2588.0) + 1.0) * 0.5;
    let g = (f64::sin(seconds / 4599.0) + 1.0) * 0.5;
    let b = (f64::sin(seconds / 1827.0) + 1.0) * 0.5;
    let color_text = format!("{:.2}", r) + "," + &format!("{:.2}", g) + "," + &format!("{:.2}", b);

    return Ok(Response::new((time_as_string + "\n" + &color_text).into()));
    // let color_text: String = {
    //     let api_response = response_guard.as_ref();
    //     if api_response.is_none() {
    //         "1.0,1.0,1.0".to_string()
    //     } else {
    //         let today = &api_response.unwrap().hourly.as_ref().unwrap().data[3];

    //         let color = match today.icon.as_ref().unwrap() {
    //             forecast::Icon::ClearDay => (1.0, 1.0, 0.15),
    //             forecast::Icon::ClearNight => (1.0, 1.0, 0.15),
    //             forecast::Icon::Rain => (0.05, 0.2, 1.0),
    //             forecast::Icon::Snow => (1.0, 1.0, 1.0),
    //             forecast::Icon::Sleet => (0.35, 0.35, 1.0),
    //             forecast::Icon::Wind => (0.5, 0.0, 1.0),
    //             forecast::Icon::Fog => (0.2, 1.0, 0.0),
    //             forecast::Icon::Cloudy => (0.4, 0.3, 0.7),
    //             forecast::Icon::PartlyCloudyDay => (1.0, 0.6, 0.4),
    //             forecast::Icon::PartlyCloudyNight => (1.0, 0.6, 0.4),
    //             forecast::Icon::Hail => (0.0, 1.0, 0.0),
    //             forecast::Icon::Thunderstorm => (-1.0, -0.5, -0.0),
    //             forecast::Icon::Tornado => (1.0, 0.0, 0.0),
    //         };
    //         color.0.to_string() + "," + &color.1.to_string() + "," + &color.2.to_string()
    //     }
    // };

    // return Ok(Response::new((time_as_string + "\n" + &color_text).into()));
}

// async fn retrieve_new_forecast() {
//     let client = Client::new();
//     let api_client = ApiClient::new(&client);
//     let forecast_req =
//         ForecastRequestBuilder::new(&CONFIG.api_key, CONFIG.lat, CONFIG.long).build();
//     let body = api_client.get_forecast(forecast_req).await.unwrap().text().await.unwrap();

//     let mut response = CURRENT_RESPONSE.lock().unwrap();
//     *response = serde_json::from_str(&body).unwrap();
// }

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("Provide exactly one argument (the port!)")
    }
    let port: u16 = args[1]
        .parse()
        .expect("The port needs to be a 16-bit integer!");

    // tokio::spawn(async {
    //     let mut interval = tokio::time::interval(std::time::Duration::from_secs(20 * 60));
    //     loop {
    //         println!("Recieving a new forecast...");
    //         retrieve_new_forecast().await;
    //         interval.tick().await;
    //     }
    // });

    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    let make_svc = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(handle)) });
    let server = Server::bind(&addr).serve(make_svc);

    println!("Starting server!");
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}
